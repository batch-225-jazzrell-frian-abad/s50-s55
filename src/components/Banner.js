// import Button from 'react-bootstrap/Button'
// import Row from 'react-bootstrap/Row'
// import Col from 'react-bootstrap/Col'

// Destructure Importation
import { Button, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom'

export default function Banner(props) {
			    return (
	                <Row>
	                	<Col id="banner">
		                    <h1 id="banner-header">Zuitt Coding Bootcamp</h1>
		                    {

		                    (props.is404) ?

		                    <div id="noPage">
		                    <h3>Zuitt Booking</h3>
		                    <h1>Page Not Found</h1>
		                    <p>Go back to the <Link to="/">Homepage</Link>.</p>	
		                    </div>

		                    :	


		                    <div id="banner-header2">
		                    <p>Opportunities for everyone, everywhere.</p>
		                    <Button variant="primary">Enroll now!</Button>
		                    </div>

		                     }
		                </Col>
	                </Row>
			    )
}